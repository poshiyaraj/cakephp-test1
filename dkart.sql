-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2020 at 11:55 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dkart`
--

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20200507082205, 'Initial', '2020-05-07 08:22:07', '2020-05-07 08:22:07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `quantity`, `price`, `image`, `description`, `created`, `modified`) VALUES
(1, 'iphone x', 'iphone-x-1', 0, '22', '1588700914-5262.png', 'iphonex', '2020-04-24 15:19:48', '2020-05-05 17:48:34'),
(6, 'Lenovo ideapad 510', 'lenovo-ideapad-510-1', 16, '1200', '1588219322-1099.png', 'Lenovo charger', '2020-04-26 11:07:28', '2020-05-06 06:00:44'),
(104, 'product 1', 'product-1-1', 1, '2', '1588671640-4773.jpeg', 'product 1', '2020-05-05 09:40:40', '2020-05-07 13:39:59'),
(134, 'alexa', 'alexa-1', 1, '1222', '1588845227-5481.jpeg', 'alexa amazo\r\n', '2020-05-07 15:09:01', '2020-05-07 15:23:47'),
(137, 'new proudct', 'new-proudct', 4, '5', '1588845252-4289.png', 'new product', '2020-05-07 15:24:12', '2020-05-07 15:24:12');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` enum('Admin','User') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Admin', '2020-04-24 11:40:26', '2020-04-24 11:40:26'),
(2, 'User', '2020-04-24 11:40:26', '2020-04-24 11:40:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(155) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `verify_token` varchar(255) NOT NULL,
  `verified` enum('0','1') NOT NULL DEFAULT '0',
  `token` varchar(255) DEFAULT NULL,
  `token_expire_time` datetime NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT 2,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `verify_token`, `verified`, `token`, `token_expire_time`, `role_id`, `created`, `modified`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$rQ4ecQQ6AeoxNljykaWDtep.gWpn8ggJwXhQVXDozZ2g1epYY4IX2', '', '1', '', '0000-00-00 00:00:00', 1, '2020-04-24 11:38:26', '2020-04-24 11:38:26'),
(2, 'Raj Poshiya', 'poshiyaraj@gmail.com', '$2y$10$XvBerdNjB0KUOEPgDDcDBubC/T26GL5jNN/.lqj4tV5r4iMSXij16', '', '0', '', '0000-00-00 00:00:00', 2, '2020-04-24 08:24:37', '2020-04-27 17:53:50'),
(4, 'newuser', 'newuser@gmail.com', '$2y$10$rQ4ecQQ6AeoxNljykaWDtep.gWpn8ggJwXhQVXDozZ2g1epYY4IX2', '', '0', '', '0000-00-00 00:00:00', 2, '2020-04-26 17:03:09', '2020-04-26 17:03:09'),
(11, 'dhruv', 'dhruviparmar98@gmail.com', '$2y$10$NnLIrixrWTrNuS1DFoKFS.IkMUe1cUbUl0tCCXQllyjM3k8M96jCO', '', '0', NULL, '0000-00-00 00:00:00', 2, '2020-04-28 04:09:07', '2020-04-28 04:09:07'),
(20, 'Raj Poshiya', 'poshiya60@gmail.com', '$2y$10$c.Vq3Mq.qr7EuW8O25axje1CyBOyJatc0scX9Jx5o79z4q9Mz7n6a', 'e00cb44fab5ba679a6d1407e8191eaf11ca38b9e', '1', '9ffcc2004bc9692d5684b470a086797a81376bd6', '2020-05-06 04:50:54', 2, '2020-05-06 04:24:05', '2020-05-06 04:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_products`
--

CREATE TABLE `user_products` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `quantity` varchar(255) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_products`
--

INSERT INTO `user_products` (`id`, `date`, `quantity`, `user_id`, `product_id`, `created`, `modified`) VALUES
(1, '2020-04-29 04:15:19', '1', 2, 1, '2020-04-26 11:44:11', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `user_products`
--
ALTER TABLE `user_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user_products`
--
ALTER TABLE `user_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_products`
--
ALTER TABLE `user_products`
  ADD CONSTRAINT `user_products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
