<?php

use Cake\Routing\Router;

?>
<div class="User form">
    <?= $this->Flash->render() ?>
    <h3>Login</h3>
    <?= $this->Form->create($loginForm) ?>

    <fieldset>
        <h4 style="display:inline-block"><?= __('Please enter your Email ID and password') ?></h4>

        <?php
        echo $this->Html->link(
            "Signup Here",
            Router::url(['_name' => 'creatUser']),
            [
                "style" => [
                    "float:right;color:#a2a251"
                ]
            ]
        );

        ?> <div style="float:right">
            <p> New Member? &nbsp; </p>
        </div>
        <br>
        <?= $this->Form->control(
            'email',
            [
                "type" => "text",
                "required" => false
            ]
        ) ?>
        <?= $this->Form->control(
            'password',
            [
                "type" => "password",
                "required" => false
            ]
        ) ?>
    </fieldset>
    <?= $this->Form->submit(__('Login')); ?>
    <?php
    echo "<br>";
    echo $this->Html->link(
        "Forgotten Password? ",
        Router::url(['_name' => 'forgotPassword']),
        ['style' => "float:right;"]
    );
    ?>
    <?= $this->Form->end() ?>
</div>