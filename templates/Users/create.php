<h2>Signup</h2>
<?php

use Cake\Routing\Router;

echo $this->Form->create($user, [
    "id" => "addUser",
    'url' => Router::url(['_name' => 'saveUser'])
]);

echo $this->Form->control("name", [
    "required" => false,
    "context" => ["validator" => "default"],
    "class" => ($this->Form->isFieldError("name")) ? "form-control is-invalid" : "form-control"
]);

echo $this->Form->control("email", [
    "required" => false,
    "type" => "text",
    "class" => ($this->Form->isFieldError("name")) ? "form-control is-invalid" : "form-control"
]);

echo $this->Form->control("password", [
    "required" => false,
    "type" => "password",
    "class" => ($this->Form->isFieldError("name")) ? "form-control is-invalid" : "form-control"

]);
echo "<br><br>";
echo $this->Form->button("Sign up", [
    "name" => "submit",
    "context" => ["validator" => "default"],
]);
echo $this->Form->end();
?>
<?php echo $this->Html->link(
    "Back to Login",
    ["controller" => "Users", "action" => "login"],
);
?>