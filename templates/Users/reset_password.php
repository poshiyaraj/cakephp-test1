<div class="users">
    <?= $this->Form->create($resetPasswordForm) ?>
    <fieldset>
        <legend><?= __('Reset password') ?></legend>

        <?= $this->Form->control('new_password', [
            'type' => 'password',
            'required' => false
        ])
        ?>
        <?= $this->Form->control('confirm_password', [
            'type' => 'password',
            'required' => false
        ]) ?>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>