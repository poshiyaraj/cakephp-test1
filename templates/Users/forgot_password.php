<?php

use Cake\Routing\Router;

?>
<div class="users form ">

    <?= $this->Form->create($forgotPasswordForm) ?>
    <fieldset>
        <legend><?= __('Forgot password') ?></legend>
        <label>Enter Your Email</label>
        <?= $this->Form->control('useremail', ['type' => 'text', 'required' => false]) ?>
    </fieldset>
    <?= $this->Form->button(__('Send')) ?>
    <?= $this->Form->end() ?>

</div>
<?php
echo $this->Html->link(
    "Back to Login",
    Router::url(['_name' => 'login'])
);
