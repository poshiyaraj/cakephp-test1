<div class="message success hidden" id="flash" onclick="this.classList.add('hidden')"></div>
<?php

use Cake\Routing\Router;

echo $this->Html->link(
    "Buy New Products",
    Router::url(['_name' => 'listProducts']),
    [
        "class" => "btn btn-info buyProduct",
        "value" => "buyProduct",
        "style" => [
            "float:right;height:36px;width:155px;margin-right:20px"
        ]
    ]
);
?>

<table class="table" id="alldata">

    <h2>Products Owned by <?= $this->getRequest()->getSession()->read('Auth.name') ?></h2>
    <thead>
        <tr>
            <th><?php echo $this->Paginator->sort("Product id") ?></th>
            <th>Name of the product</th>
            <th>Product Image</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($userData as $key => $val) {
        ?>
            <tr>
                <td><?= $val->product->id ?></td>
                <td><?= $val->product->name ?></td>
                <td><?php echo $this->Html->image("uploads/" . $val->product->image, [
                        'width' => 100,
                        'heigth' => 100
                    ]); ?></td>
                <td><?= $val->date ?></td>
                <td><?php
                    echo $this->Html->link(
                        "Delete",
                        ["controller" => "UserProducts", "action" => "removeFromCart"],
                        [
                            "value" => "removeFromCart",
                            "class" => "btn btn-danger action",
                            "style" => "margin-left:5px",
                            "data-id" => $val->id,
                            "data-url" => Router::url(['_name' => 'removeFromCart', $val->id]),
                        ]
                    );
                    ?></td>
            </tr>
        <?php

        }
        ?>
    </tbody>
</table>
<?= $this->element('pagination') ?>