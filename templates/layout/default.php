<?php

?>
<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        Dkart
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <?= $this->Html->css('milligram.min.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->script("bootbox.min.js") ?>
    <?= $this->Html->script("bootbox.locales.min.js") ?>
    <?= $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken')); ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <nav class="top-nav">
        <div class="top-nav-title">
            <a href="/"><span>D</span>Kart</a>
        </div>
        <div>
            <?php
            if ($this->getRequest()->getSession()->read('Auth')) {
                echo $this->getRequest()->getSession()->read('Auth.name');
            ?>
                <button type="button" class="btn btn-default btn-sm" style="background-color:#ff9900">
                    <span class="glyphicon glyphicon-off">
                        <?php
                        echo $this->Html->link(
                            "Logout",
                            ["controller" => "Users", "action" => "logout"],
                            [
                                "class" => " logout",
                                "value" => "logout",
                                "style" => []
                            ]
                        );
                        ?>
                    </span>
                </button>
            <?php
            }
            ?>
        </div>
    </nav>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
    </footer>
    <?= $this->Html->script('action.js') ?>
    <?= $this->Html->script('photo_preview.js') ?>


</body>

</html>