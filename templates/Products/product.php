<?php

echo "<h3>Add product</h3>";
error_reporting(0);
if ($create) {
    echo $this->Form->create($create, [
        'url' => ["action" => "save"],
        "type" => "file",
        "id" => "addProduct",
    ]);
} else if ($edit) {
    echo $this->Form->create($edit, [
        'url' => ["action" => "update"],
        "type" => "file",
        "id" => "addProduct",
    ]);
}
echo $this->Form->hidden("id", []);

echo $this->Form->control("name", [
    'autocomplete' => "off",
    'type' => 'text',
    'escape' => false,
    "required" => false,
    "class" => ($this->Form->isFieldError("name")) ? "form-control is-invalid" : "form-control"
]);
echo $this->Form->control("quantity", [
    'autocomplete' => "off",

    "type" => "text",
    "required" => false,
    "class" => ($this->Form->isFieldError("quantity")) ? "form-control is-invalid" : "form-control"
]);
echo $this->Form->control("price", [
    'autocomplete' => "off",
    "type" => "text",
    "required" => false,
    "context" => ["validator" => "default"],
]);
echo $this->Form->control("product_photo", [
    "id" => "file",
    "type" => "file",
    "alt" => "No image for product",
    "required" => false,
]);
if ($edit->image) {
    echo $this->Html->image("uploads/" . $edit->image, [
        "alt" => "No image for product",
        "style" => "margin-top:10px",
        "height" => 100,
        "width" => 100
    ]);
}

echo $this->Form->control("description", [
    'autocomplete' => "off",
    "escape" => false,
    "type" => "textarea",
    "required" => false,
    "class" => ($this->Form->isFieldError("description")) ? "form-control is-invalid" : "form-control"
], ['escape' => false]);
echo $this->Form->button("Submit", [
    "name" => "submit",
]);
echo $this->Form->end();
