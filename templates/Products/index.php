<div class="message success hidden" id="flash" onclick="this.classList.add('hidden')"></div>

<div class="details-container" id="details-container">

    <?php

    use Cake\Routing\Router;

    echo $this->Flash->render("message");
    $userrole = $this->getRequest()->getSession()->read('Auth.role_id');

    if ($userrole == 1) {
        echo $this->Html->link(
            "AddProduct",
            Router::url(['_name' => 'addProduct']),
            [
                "style" => [
                    "float:right;height:36px;width:160px;margin-right:20px;"
                ],
                "class" => "btn btn-danger mycart",
                "value" => "userCart",
            ]
        );
    }
    if ($userrole != 1) {
    ?>
        <button type="button" class="btn btn-info btn-lg" style="float: right;color:#116CFE">
            <span class="glyphicon glyphicon-shopping-cart">
            <?php
            echo $this->Html->link(
                "MyCart",
                Router::url(['_name' => 'userCart']),
                [
                    "class" => "  mycart",
                    "value" => "userCart",
                ]
            );
        }
            ?>
            </span>
        </button>

        <table class="table" id="alldata">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort("id") ?></th>
                    <th>Name</th>
                    <th><?php echo $this->Paginator->sort('price'); ?></th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($products as $key => $product) {
                ?>
                    <tr>
                        <td><?= $product->id ?></td>
                        <td><?= $product->name ?></td>
                        <td><?php echo $this->Number->currency($product->price); ?></td>
                        <td><?php echo $this->Html->image("uploads/" . $product->image, [
                                'width' => 100,
                                'heigth' => 100
                            ]); ?></td>
                        <td><?= $product->description ?></td>
                        <td>
                            <?php
                            if ($userrole == 1) {
                                echo $this->Html->link(
                                    "Edit",
                                    ["action" => "edit", $product->slug],
                                    [
                                        "class" => "glyphicon glyphicon-pencil",
                                        "value" => "editProduct",
                                        "data-name" => $product->slug,
                                        "style" => "width:67px",
                                    ]
                                );
                                echo $this->Html->link(
                                    "Delete",
                                    [null],
                                    [
                                        "value" => "deleteProduct",
                                        "class" => "glyphicon glyphicon-trash action",
                                        "style" => "margin-left:5px",
                                        "data-url" => Router::url(['_name' => 'deleteProduct', $product->slug]),

                                    ]
                                );
                            } else {
                                if ($product->quantity == 0) {
                                    echo "<button type='button' style='background-color:#463a3b6b;border:#969393'
                                     disabled>Out of stock</button>";
                                } else {
                                    echo $this->Html->link(
                                        "BuyNow",
                                        [
                                            'controller' => 'Users',
                                            'action' => 'index'
                                        ],
                                        [
                                            "class" => "glyphicon glyphicon-briefcase action",
                                            "value" => "buyNow",
                                            "style" => "color:#ad6409",
                                            "data-url" => Router::url(['_name' => 'addToCart', $product->slug]),
                                        ]
                                    );
                                }
                            }
                            ?>
                        </td>
                    </tr>

                <?php
                }
                ?>
            </tbody>
        </table>
        <?= $this->element('pagination') ?>