<?php
$paginator = $this->Paginator->setTemplates([
    'number' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'first' => '<li class="page-item"><a href="{{url}}" class="page-link">&laquo;</a></li>',
    'last' => '<li class="page-item"><a href="{{url}}" class="page-link">&raquo;</a></li>',
    'prevActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&lt;</a></li>',
    'nextActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&gt;</a></li>'
]);

?>

<nav>
    <ul class="pagination">
        <?php
        echo $this->paginator->first();
        if ($this->paginator->hasPrev()) {
            echo $paginator->prev();
        }
        echo $this->paginator->numbers();
        if ($this->paginator->hasNext()) {
            echo $this->paginator->next();
        }
        echo $this->paginator->last();
        ?>
    </ul>
</nav>