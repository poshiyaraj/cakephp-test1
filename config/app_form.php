<?php
return [
    'inputContainer' => '<div class="form-group">{{content}}</div>',
    'inputContainerError' => '<div class="form-group {{required}} error">{{content}}{{error}}</div>',
    'option' => '<option value="{{value}}"{{attrs}}>{{text}}</option>',
    'file' => '<input type="file" name="{{name}}"{{attrs}}>',
    'error' => '<div class="invalid-feedback text-danger">{{content}}</div>',
];
