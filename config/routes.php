<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

$routes->setRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->connect(
        '/',
        ['controller' => 'Users', 'action' => 'login'],
        ['_name' => 'login']
    );
    $routes->connect(
        'user/index',
        ['controller' => 'Users', 'action' => 'index'],
        ['_name' => 'userIndex']
    );
    $routes->connect(
        'users/create',
        ['controller' => 'Users', 'action' => 'create'],
        ['_name' => 'creatUser']
    );
    $routes->connect(
        'users/save',
        ['controller' => 'Users', 'action' => 'save'],
        ['_name' => 'saveUser']
    );
    $routes->connect(
        'users/update',
        ['controller' => 'Users', 'action' => 'update'],
        ['_name' => 'updateUser']
    );
    $routes->connect(
        'users/logout',
        ['controller' => 'Users', 'action' => 'logout'],
        ['_name' => 'logout']
    );
    $routes->connect(
        'users/forgotPassword',
        ['controller' => 'Users', 'action' => 'forgotPassword'],
        ['_name' => 'forgotPassword']
    );
    $routes->connect(
        'users/verification/{token}',
        ['controller' => 'Users', 'action' => 'verifyUser'],
        ['_name' => 'verifyUser']
    )
        ->setPass(['token']);
    $routes->connect(
        'users/resetpassword/{token}',
        ['controller' => 'Users', 'action' => 'resetPassword'],
        ['_name' => 'resetPassword']
    )
        ->setPass(['token']);
});

Router::scope('/products', function (RouteBuilder $routes) {
    $routes->connect(
        '/index/*',
        ['controller' => 'Products', 'action' => 'index'],
        ['_name' => 'listProducts']
    );
    $routes->connect(
        '/create',
        ['controller' => 'Products', 'action' => 'create'],
        ['_name' => 'addProduct']
    );
    $routes->connect(
        '/save',
        ['controller' => 'Products', 'action' => 'save'],
        ['_name' => 'saveProduct']
    );
    $routes->connect(
        '/edit/**',
        ['controller' => 'Products', 'action' => 'edit'],
        ['_name' => 'editProduct']
    );
    $routes->connect(
        '/update',
        ['controller' => 'Products', 'action' => 'update'],
        ['_name' => 'updateProduct']
    );
    $routes->connect(
        '/delete/**',
        ['controller' => 'Products', 'action' => 'delete'],
        ['_name' => 'deleteProduct']
    );
});
Router::scope('/userproducts', function (RouteBuilder $routes) {
    $routes->connect(
        '/',
        ['controller' => 'UserProducts', 'action' => 'index'],
        ['_name' => 'userCart']
    );
    $routes->connect(
        '/buynow/*',
        ['controller' => 'UserProducts', 'action' => 'buyNow'],
        ['_name' => 'addToCart']
    );
    $routes->connect(
        '/removefromcart/*',
        ['controller' => 'UserProducts', 'action' => 'removeFromCart'],
        ['_name' => 'removeFromCart']
    );
});
