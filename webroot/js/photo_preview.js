$(document).ready(function () {
    function imagepreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#updatedetail  img').remove();
                $('#addProduct  img').remove();
                $('#file').after('<img src="' + e.target.result + '"style="margin-top: 10px" width="100" height="100"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on("change", "#file", function () {
        imagepreview(this);
    })
});