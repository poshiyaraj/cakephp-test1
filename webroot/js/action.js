
$(document).on("click", ".action", function (e) {
    e.preventDefault()
    var csrfToken = $('meta[name="csrfToken"]').attr('content');
    console.log(csrfToken);

    var action = $(this).attr("value");
    var url = $(this).data('url')
    if (action == "deleteProduct") {
        let comment = "Are you sure you want to delete this product?";
        confirm(comment, url, csrfToken);
    } else if (action == "buyNow") {
        let comment = "Are you sure you want to add this product to your cart";
        confirm(comment, url, csrfToken);

    } else if (action == "removeFromCart") {
        let comment = "Are you sure you want to remove this product your cart";
        confirm(comment, url, csrfToken);
    }
});

function confirm(comment, url, csrfToken) {
    bootbox.confirm(comment, function (result) {
        if (result) {
            $.ajax({
                "url": url,
                "type": "post",
                "headers": {
                    'X-CSRF-Token': csrfToken
                },
                success: function (result) {
                    displayFlash(result);
                    $("table").load(window.location.href + " table");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            })
        }
    })
}

function displayFlash(result) {
    var response = JSON.parse(result);
    if (response.status == "error") {
        $("#flash").attr("class", "message error hidden");
    }
    $("#flash").html(response.message);
    $("#flash").toggleClass("hidden");
    setTimeout(function () {
        $('#flash').toggleClass('hidden');
    }, 2000);

}