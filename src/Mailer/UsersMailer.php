<?php
declare(strict_types=1);

namespace App\Mailer;

use Cake\Mailer\Mailer;

class UsersMailer extends Mailer
{
    /**
     * This method is use to send mail when user appliying for forgot password
     *
     * @param array $data data to be send to
     * @return $this
     */
    public function forgotPassword($data)
    {
        return $this
            ->setEmailFormat('html')
            ->setTransport('gmail')
            ->setTo($data->getData('email'))
            ->setSubject("Password Reset")
            ->deliver('Please click the following link to rest your password
                 <a href="http://localhost:8765/users/resetpassword/'
                . $data->getData('token') . '">Reset Password</a>');
    }

    /**
     * This method is use to send mail to verify user
     *
     * @param array $data data to be send to
     * @return $this
     */
    public function verifyUser($data)
    {
        return $this
            ->setEmailFormat('html')
            ->setTransport('gmail')
            ->setTo($data->getData('email'))
            ->setSubject("Please Verify its you")
            ->deliver('Please click the following link to Verify your account
                <a href="http://localhost:8765/users/verification/'
                . $data->getData('verify_token') . '">Verify Account</a>');
    }
}
