<?php

declare(strict_types=1);

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ResetPasswordForm extends Form
{
    /**
     * This method is use to determine schema for form reset password
     * which have 2 fields new_password and confirm password
     * @param \App\Form\Cake\Form\Schema $schema schema class instance
     * @return $this returns schema information as defineds
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema
            ->addField('new_password', ['type' => 'string'])
            ->addField('confirm_password', ['type' => 'string']);
    }

    /**
     * This method is used to apply validation rules for modeless form
     * both password should match and should of 8 or morgits also must be alphanumric
     * @param \App\Form\Cake\Validation\Validator $validator validater class instance
     * @return \App\Form\Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('new_password', "PLease Provide Your Password")
            ->notEmptyString("new_password", "Please Provide Your Password ")
            ->add('new_password', [
                'length' => [
                    'rule' => ['minLength', 8],
                    'message' => 'Please enter atleast 8 characters in password your password.',
                    'last' => true,
                ],
            ])
            ->add('new_password', [
                'alphaNumeric' => [
                    'rule' => 'alphaNumeric',
                    'message' => 'Password need to be Alphanumric',
                    'last' => true,
                ],
            ])
            ->regex(
                'new_password',
                '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/',
                "Password must contains uppercase, lowercase , digits and special characters !"
            );

        $validator
            ->requirePresence('confirm_password', "Please Provide confirm Password")
            ->notEmptyString("confirm_password", "Please Provide confirm Password ")
            ->add('confirm_password', [
                'match' => [
                    'rule' => ['compareWith', 'new_password'],
                    'message' => 'Sorry! Password dose not match. Please try again!',
                    'last' => true,
                ],
            ]);

        return $validator;
    }

    /**
     * These method is executed only if the data passed is valid as per the defined rules
     * @param array $data privded data
     * @return bool
     */
    protected function _execute(array $data): bool
    {
        return true;
    }
}
