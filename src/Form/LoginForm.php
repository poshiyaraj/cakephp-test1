<?php
declare(strict_types=1);

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class LoginForm extends Form
{
    /**
     * This method is use to determine schema for login fomr
     * It will have 2 fields email and password
     * @param \App\Form\Cake\Form\Schema $schema schema class instance
     * @return $this returns schema information as defineds
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema
            ->addField('email', ['type' => 'string'])
            ->addField('password', ['type' => 'string']);
    }

    /**
     * This method is used to apply validation rules for modeless form
     * Checking valid email formet
     * @param \App\Form\Cake\Validation\Validator $validator validater class instance
     * @return \App\Form\Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString("email", "Please enter your EmailID ")

            ->add(
                "email",
                [
                "valid_email" => [
                    "rule" => ["email"],
                    "message" => "Please enter valid email format",
                    'last' => true,
                ],
                ]
            );

        $validator
            ->notEmptyString("password", "Please Provide Your Password ");

        return $validator;
    }

    /**
     * These method is executed only if the data passed is valid as per the defined rules
     * @param array $data privded data
     * @return bool
     */
    protected function _execute(array $data): bool
    {
        return true;
    }
}
