<?php
declare(strict_types=1);

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ForgotPasswordForm extends Form
{
    /**
     * This method is use to determine schema for ForgotPasswordForm
     * This schema will contain 1 field that is email
     * @param \App\Form\Cake\Form\Schema $schema schema class instance
     * @return $this returns schema information as defineds
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema
            ->addField('useremail', ['type' => 'string']);
    }

    /**
     * This method is used to apply validation rules for modeless form
     * Checking is email format is valid or not
     * @param \App\Form\Cake\Validation\Validator $validator validater class instance
     * @return \App\Form\Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString("useremail", "Please enter your EmailID ")
            ->requirePresence("useremail", "Please enter your EmailID")
            ->add(
                "useremail",
                [
                "valid_email" => [
                    "rule" => ["email"],
                    "message" => "Please enter valid email format",
                    'last' => true,
                ],
                ]
            );

        return $validator;
    }

    /**
     * These method is executed only if the data passed is valid as per the defined rules
     * @param array $data privded data
     * @return bool
     */
    protected function _execute(array $data): bool
    {
        return true;
    }
}
