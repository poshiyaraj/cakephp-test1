<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;

class FileComponent extends Component
{
    public $filename = '';
    public $path = "";

    /**
     * Saves uploaded file uploaded by user to the location given in $path
     * @param \App\Controller\Component\file $file File to be uploaded by user
     * @param string $path Full path where file must be moved
     * @return void
     */
    public function uploadFile($file, $path)
    {
        $this->setFileName($file);
        $this->setPath($path);
        $path = $this->path . $this->filename;
        $move = $file->moveTo($path);
    }

    /**
     * Sets the path for storing file
     *
     * @param \App\Controller\Component\file $path Set full path of a file to be uploaded
     * @return void
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Checks if there are any errors in uploading file
     *
     * @param \App\Controller\Component\file $file File to be uploaded by user
     * @return array
     *
     */
    public function checkFileError($file)
    {
        return $file->getError();
    }

    /**
     * Sets the file name to  variable $filename genetaing a unique name for a file
     *
     * @param \App\Controller\Component\file $file File to be uploaded by user
     * @return void
     */
    public function setFileName($file)
    {
        $filename = $file->getClientFilename();
        $filetype = $file->getClientMediaType();
        $filetype = ltrim($filetype, "image/");
        $number = rand(1000, 9999);
        $time = time();
        $filename = $time . "-" . $number . "." . $filetype;
        $this->filename = $filename;
    }

    /**
     * This method reutrns filename uploaded by user
     * @return string
     */
    public function getFileName()
    {
        return $this->filename;
    }

    /**
     * This method deletes a file from the path passed in $path
     * @param \App\Controller\Component\file $file File uploaded by user
     * @param string $path Full path to the location of file
     * @return void
     */
    public function deleteFile($file, $path)
    {
        $delete = $path . $file;
        unlink($delete);
    }
}
