<?php

declare(strict_types=1);

namespace App\Controller;

use App\Event\UsersListner;
use App\Form\ForgotPasswordForm;
use App\Form\LoginForm;
use App\Form\ResetPasswordForm;
use Cake\Event\EventInterface;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\Utility\Security;

class UsersController extends AppController
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel("UserProducts");
        $this->loadModel("Roles");
    }

    /**
     * Called before the controller action. You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @param \Cake\Event\EventInterface $event An Event instance
     * @return void
     */
    public function beforeFilter(EventInterface $event): void
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(
            [
                'verifyUser', 'login', 'create', 'save', 'forgotPassword', 'resetPassword',
            ]
        );
    }

    /**
     * user login method
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function login()
    {
        $loginForm = new LoginForm();
        $this->set('loginForm', $loginForm);
        $result = $this->Authentication->getResult();
        if ($this->request->is('post')) {
            if ($loginForm->execute($this->request->getData())) {
                if ($result->isValid()) {
                    if ($result->getData()->verified == "1") {

                        return $this->redirect(Router::url(['_name' => 'listProducts']));
                    }
                    $this->Authentication->logout();
                    $this->Flash->error(__('Please verify your account link with your email'), ['clear' => true]);

                    return $this->redirect(["action" => "login"]);
                }
                $this->Flash->error(__('Invalid Email Id or Password'), ['clear' => true]);
            }
        }
    }

    /**
     * user logout method
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function logout()
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Authentication->logout();

            return $this->redirect(["action" => "login"]);
        }
    }

    /**
     * This method allows new user to signup
     *
     * @return void
     */
    public function create(): void
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEmptyEntity();
        $this->set("user", $user);
    }

    /**
     * This method is used to save new user
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function save()
    {
        $this->autoRender = false;

        $data = $this->getRequest()->getData();
        $user = $this->Users->newEmptyEntity();
        $user = $this->Users->patchEntity($user, $data);
        $this->set("user", $user);
        if ($user->getErrors()) {
            $this->render('create');
        } else {
            $event = new UsersListner();

            $verifyToken = Security::hash(Security::randomBytes(25));
            $user->verify_token = $verifyToken;

            if ($this->Users->save($user)) {
                $this->Users->sendSignupMail($user->toArray());
                $this->Flash->success(__('Account created Successfully'), ['clear' => true]);

                return $this->redirect(["action" => "login"]);
            }
            $this->Flash->error(__("Something went wrong while saving your details"));
        }
    }

    /**
     * This method is use to verify newly signed up user
     *
     * @param string $token A unique token genereated on signning up user
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function verifyUser($token)
    {
        $this->autoRender = false;
        $where = ['verify_token' => $token];
        $verify = $this->Users->findFirst($where);
        $verify->verified = 1;
        if ($this->Users->save($verify)) {
            $this->Flash->success(__("Your account has successfully verified"), ['clear' => true]);

            return $this->redirect(['action' => "login"]);
        }
        $this->Flash->error(__("Invalid Request"));
    }

    /**
     * This method is to help user in case of forget password
     *
     * @return void
     */
    public function forgotPassword(): void
    {
        $event = new UsersListner();
        $this->Users->getEventManager()->on($event);
        $forgotPasswordForm = new ForgotPasswordForm();
        $this->set('forgotPasswordForm', $forgotPasswordForm);
        if ($this->request->is('post')) {
            if ($forgotPasswordForm->execute($this->request->getData())) {
                $userEmail = $this->getRequest()->getData('useremail');
                $tokenExpireTime = new Time('+10 minutes');
                $token = Security::hash(Security::randomBytes(25));

                $where = ['email' => $userEmail];
                $user = $this->Users->findFirst($where);
                if ($user) {
                    $user->token = $token;
                    $user->token_expire_time = $tokenExpireTime;

                    if ($this->Users->save($user)) {
                        $this->Flash->success(
                            __('Rest Password was sent to your email,Please check your email'),
                            ['clear' => true]
                        );
                        $this->Users->sendForgotPasswordMail($user->toArray(76));
                    }
                } else {
                    $this->Flash->error(__("No account found linked with this email Id"));
                }
            }
        }
    }

    /**
     * This method is use to verify newly signed up user
     *
     * @param string $token A unique token genereated when user has forgotten password
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function resetPassword($token)
    {
        $where = ['token' => $token];
        $existUser = $this->Users->findFirst($where);
        $currentTime = Time::now();
        $resetPasswordForm = new ResetPasswordForm();
        $this->set('resetPasswordForm', $resetPasswordForm);
        if ($existUser && ($currentTime <= $existUser->token_expire_time)) {
            if ($resetPasswordForm->execute($this->request->getData())) {
                $existUser->password = $this->getRequest()->getData('new_password');
                $existUser->token = null;
                if ($this->Users->save($existUser)) {
                    $this->Flash->success(
                        __("Password Changed Successfully"),
                        ['clear' => true]
                    );

                    return $this->redirect(['_name' => "login"]);
                }
            }
        } else {
            $this->Flash->error(__("Invalid Request or link has been expired"));

            return $this->redirect(["action" => 'login']);
        }
    }
}
