<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Routing\Router;

class UserProductsController extends AppController
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $recordPerPage = 3;
        $this->paginate = [
            'limit' => $recordPerPage,
        ];
    }

    /**
     * This method give us the exixting data of prdoucts
     *
     * @return void
     */
    public function index(): void
    {
        $userId = $this->getRequest()->getSession()->read('Auth.id');
        $userEntity = $this->UserProducts->find('all')->where(["user_id" => $userId])->contain(['Products']);
        $this->set('userData', $this->paginate($userEntity));
    }

    /**
     * Use to add a product to user's cart
     *
     * @param string $productSlug unique product slug name
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function buyNow($productSlug)
    {
        $this->Authorization->skipAuthorization();
        $this->autoRender = false;

        $product = $this->UserProducts->Products->findBySlug($productSlug)->first();
        $productQuantity = $product["quantity"];
        $productId = $product['id'];
        $userId = $this->getRequest()->getSession()->read('Auth.id');

        $boughtProduct = $this->UserProducts->newEmptyEntity();
        $data = ["user_id" => $userId, "product_id" => $productId, "date" => FrozenTime::now()];
        $boughtProduct = $this->UserProducts->patchEntity($boughtProduct, $data);
        if ($productQuantity >= 1) {
            if ($this->UserProducts->save($boughtProduct)) {
                $product->quantity = --$productQuantity;
                $this->UserProducts->Products->save($product);
                if ($this->request->is('ajax')) {
                    $data = ['status' => 'success', 'message' => 'Product bought succesfully'];
                    $response = json_encode($data);

                    $this->autoRender = false;
                    $this->response = $this->response->withStringBody($response);

                    return $this->response;
                }
            }
            $this->Flash->error(__('There was a error in buying the product'), ['clear' => true,]);
        }
        $this->Flash->error(__('Product is out of stock'), ['clear' => true,]);

        return $this->redirect(Router::url(['_name' => 'listProducts']));
    }

    /**
     * Removes product from cart
     *
     * @param int $productId Product id for product in users cart
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function removeFromCart($productId)
    {
        $this->autoRender = false;
        $this->Authorization->skipAuthorization();
        $userProduct = $this->UserProducts->findById($productId)->first();
        $orignalId = $userProduct->product_id;
        $product = $this->UserProducts->Products->findById($orignalId)->first();

        $user = $this->request->getAttribute('authentication');
        if ($this->UserProducts->delete($userProduct)) {
            ++$product->quantity;
            $this->UserProducts->Products->save($product);
            if ($this->request->is('ajax')) {
                $data = ['status' => 'success', 'message' => 'Product Deleted from cart succesfully'];
                $response = json_encode($data);

                $this->response = $this->response->withStringBody($response);

                return $this->response;
            }
        }
        $this->Flash->error(__("Something went wrong while removing the product"), ["clear" => true]);
    }
}
