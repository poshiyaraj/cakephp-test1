<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;

class ProductsController extends AppController
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel("UserProducts");
        $this->loadComponent('Paginator');
        $this->loadComponent('File');

        $recordPerPage = 3;
        $this->paginate = [
            'limit' => $recordPerPage,
        ];
    }

    /**
     * Used to get existing data of products
     *
     * @return void
     */
    public function index(): void
    {
        try {
            $this->paginate();
            $this->set('products', $this->paginate($this->Products->find("all")));
        } catch (NotFoundException $e) {
            $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Used to Create new products with authorization
     *
     * @return void
     */
    public function create(): void
    {
        $product = $this->Products->newEmptyEntity();
        if ($this->Authorization->can($product, 'create')) {
            $this->viewBuilder()->setTemplate('product');
            $this->set("create", $product);
        } else {
            $this->Flash->error(__("You  Dont have suffecent rights"), ['clear' => true,]);
        }
    }

    /**
     * This method is used to save created product
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function save()
    {

        $this->autoRender = false;
        $data = $this->getRequest()->getData();
        $product = $this->Products->newEmptyEntity();
        $product = $this->Products->patchEntity($product, $data);
        $this->viewBuilder()->setTemplate('product');
        $this->set("create", $product);

        if (!empty($product->getErrors())) {
            return $this->render("create");
        }
        $this->File->uploadFile($data['product_photo'], UPLOADS);
        $filename = $this->File->getFileName();
        $product->image = $filename;
        if ($this->Products->save($product)) {
            $this->Flash->success(__('Product Added Successfully '), ['clear' => true,]);

            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Something went wrong on Adding new product '), ['clear' => true,]);
    }

    /**
     * Use to edit Products
     *
     * @param string $productSlug unique product slug name
     * @return void
     */
    public function edit($productSlug): void
    {
        $product = $this->Products->findBySlug($productSlug)->first();
        if ($this->Authorization->can($product, 'edit')) {
            $this->viewBuilder()->setTemplate('product');
            $this->set('edit', $product);
        } else {
            $this->Flash->error(__("You are Dont have suffecent rights"), ['clear' => true,]);
        }
    }

    /**
     * Use to update edited products
     *
     * @return \App\Controller\Cake\Http\Respoonse|null
     */
    public function update()
    {
        $this->autoRender = false;
        $formdata = $this->getRequest()->getData();
        $product = $this->Products->get($formdata["id"]);
        $product = $this->Products->patchEntity($product, $formdata);

        if ($this->File->checkFileError($formdata['product_photo']) == 0) {
            $uploadFile = $this->File->uploadFile($formdata['product_photo'], UPLOADS);
            $filename = $this->File->getFileName();
            $deleteFile = $this->File->deleteFile($product->image, UPLOADS);
            $product->image = $filename;
        }

        if (!empty($product->getErrors())) {
            $this->viewBuilder()->setTemplate('product');
            $this->set("edit", $product);

            return $this->render("edit");
        }
        if ($this->Products->save($product)) {
            $this->Flash->success(__('Product Updated succesfully'), ['clear' => true,]);

            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Product Updation failed'), ['clear' => true,]);
        }
    }

    /**
     * This method is used to delete product
     *
     * @param string $productSlug Unique slug name of a product
     * @return \App\Controller\Cake\Http\Respoonse|null
     */
    public function delete($productSlug)
    {
        $this->autoRender = false;
        $product = $this->Products->findBySlug($productSlug)->first();
        if ($this->Authorization->can($product, 'delete')) {
            $filename = $product->image;
            $this->File->deleteFile($filename, UPLOADS);
            $this->Products->delete($product);
            if ($this->request->is('ajax')) {
                $data = ['status' => 'success', 'message' => 'Product Deleted succesfully'];
                $response = json_encode($data);

                $this->response = $this->response->withStringBody($response);

                return $this->response;
            }
        }
        $this->Flash->error(__("You are Dont have suffecent rights"), ['clear' => true,]);
    }
}
