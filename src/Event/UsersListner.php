<?php
declare(strict_types=1);

namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\Mailer\MailerAwareTrait;

class UsersListner implements EventListenerInterface
{
    use MailerAwareTrait;

    /**
     * list of implemented events
     * @return array
     */
    public function implementedEvents(): array
    {
        return [
            'signup' => 'signupUserMail',
            'forgotPassword' => 'sendForgotMail',
        ];
    }

    /**
     * This method sends a mail to user after signup
     *
     * @param array $data Details to sent in email
     * @return void
     */
    public function signupUserMail($data): void
    {
        $this->getMailer('Users')->send('verifyUser', [$data]);
    }

    /**
     * These method sends mail if user hase forgot password
     *
     * @param array $data Details to sent in email
     * @return void
     */
    public function sendForgotMail($data): void
    {
        $this->getMailer('Users')->send('forgotPassword', [$data]);
    }
}
