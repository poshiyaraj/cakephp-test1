<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Product extends Entity
{
    protected $_accessible = [
        "name" => true,
        "quantity" => true,
        "price" => true,
        "image" => true,
        "description" => true,
    ];
}
