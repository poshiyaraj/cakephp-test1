<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity
{
    /**
     * This method is used to convert password provided by user into hash form
     *
     * @param string $password Password provided by user
     * @return string
     */
    protected function _setPassword(string $password): ?string
    {
        if (strlen($password) > 0) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($password);
        }
    }

    protected $_accessible = [
        "name" => true,
        "email" => true,
        "password" => true,
        "role_id" => true,
        "token" => true,
        "verify_token" => true,
    ];
}
