<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Table;

class RolesTable extends Table
{
    /**
     * Initialize a table instance. Called after the constructor.
     *
     * You can use this method to define associations, attach behaviors
     * define validation and do any other initialization logic you need.
     *
     * @param array $config Configuration options passed to the constructor
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->addBehavior("Timestamp");
    }
}
