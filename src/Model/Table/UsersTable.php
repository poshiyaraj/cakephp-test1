<?php

declare(strict_types=1);

namespace App\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    /**
     * Initialize a table instance. Called after the constructor.
     *
     * You can use this method to define associations, attach behaviors
     * define validation and do any other initialization logic you need.
     *
     * @param array $config Configuration options passed to the constructor
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior("Timestamp");
        $this->belongsToMany(
            'Products',
            [
                'joinTable' => 'user_products',
            ]
        );
    }

    /**
     * This mehtod lets you manipulate the request data just before entities are created
     *
     * Use of this method is to santized input data submited by user
     *
     * @param \App\Model\Table\Cake\Event\Event $event instance of Cake\Event\Event class
     * @param \ArrayObject $data The $data parameter is an ArrayObject instance,
     * so you don’t have to return it to change the data used to create entities.
     * @param \ArrayObject $options provide options if needed
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options): void
    {
        if (isset($data)) {
            foreach ($data as $key => $val) {
                if ($key != "product_photo") {
                    $data[$key] = h($val);
                }
            }
        }
    }

    /**
     * This metod valdidates the data provided by user
     *
     * You can add rules here to validate your data
     *
     * @param \App\Model\Table\Cake\Validation\Validator $validator instance of Cake\Validation\Validator class
     * @return \App\Model\Table\Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence("name", "Please enter your name")
            ->notEmptyString("name", "Please enter your name ");

        $validator
            ->notEmptyString("email", "Please enter your EmailID ")
            ->requirePresence("email", "Please enter your EmailID")
            ->add(
                "email",
                [
                    "valid_email" => [
                        "rule" => ["email"],
                        "message" => "Please enter valid email format",
                        'last' => true,
                    ],
                ]
            )
            ->add(
                "email",
                [
                    "unique_email" => [
                        "rule" => "validateUnique",
                        "provider" => "table",
                        "message" => "Email already exist",
                    ],
                ]
            );

        $validator
            ->requirePresence("password", "Please Provide Your Password")
            ->notEmptyString("password", "Please Provide Your Password ")
            ->add(
                'password',
                [
                    'length' => [
                        'rule' => ['minLength', 8],
                        'message' => 'Password need to be at least 8 characters long',
                        'last' => true,
                    ],
                ]
            )
            ->add(
                'password',
                [
                    'alphaNumeric' => [
                        'rule' => 'alphaNumeric',
                        'message' => 'Password need to be Alphanumric',
                        'last' => true,
                    ],
                ]
            )
            ->regex(
                'password',
                '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/',
                "Password must contains uppercase, lowercase , digits and special characters !"
            );

        return $validator;
    }

    /**
     * This method is called when user signups new account
     *
     * @param array $user User entity in for of array
     * @return bool
     */
    public function sendSignupMail($user)
    {
        $event = new Event('signup', $this, $user);
        if ($this->getEventManager()->dispatch($event)) {
            return true;
        }

        return false;
    }

    /**
     * This method is called when user signups new account
     *
     * @param array $user User Entity in form of array
     * @return bool
     */
    public function sendForgotPasswordMail($user)
    {
        $event = new Event('forgotPassword', $this, $user);
        if ($this->getEventManager()->dispatch($event)) {
            return true;
        }

        return false;
    }

    /**
     * This is a genreilzed method to where query for users table
     *
     * @param array $where array of conditions to apply where clause
     * @return \App\Model\Table\Cake\ORM\Entity
     */
    public function findFirst($where)
    {
        return $this->find('all')->where($where)->first();
    }
}
