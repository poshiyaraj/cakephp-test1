<?php
declare(strict_types=1);

namespace App\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table
{
    /**
     * Initialize a table instance. Called after the constructor.
     *
     * You can use this method to define associations, attach behaviors
     * define validation and do any other initialization logic you need.
     *
     * @param array $config Configuration options passed to the constructor
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior("ProductSlug");
        $this->addBehavior("Timestamp");
        $this->belongsToMany(
            'Users',
            [
                'joinTable' => 'user_products',
            ]
        );
    }

    /**
     * This mehtod lets you manipulate the request data just before entities are created
     *
     * Use of this method is to santized input data submited by user
     *
     * @param \App\Model\Table\Cake\Event\Event $event instance of Cake\Event\Event class
     * @param \ArrayObject $data The $data parameter is an ArrayObject instance,
     * so you don’t have to return it to change the data used to create entities.
     * @param \ArrayObject $options provide options if needed
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options): void
    {
        if (isset($data)) {
            foreach ($data as $key => $val) {
                if ($key != "product_photo") {
                    $data[$key] = h($val);
                }
            }
        }
    }

    /**
     * This metod valdidates the data provided by user
     *
     * You can add rules here to validate your data
     *
     * @param \App\Model\Table\Cake\Validation\Validator $validator instance of Cake\Validation\Validator class
     * @return \App\Model\Table\Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence("name", "Please provide product name")
            ->notEmptyString("name", "Please provide product name ");
        $validator
            ->requirePresence("price", "Please provide Price of the product")
            ->notEmptyString("price", "Please provide  Price of the product ")
            ->add(
                "price",
                [
                    "numeric" => [
                        "rule" => "numeric",
                        'last' => true,
                        "message" => "Price must be numeric",
                    ],
                ]
            );
        $validator
            ->requirePresence("quantity", "Please provide quantity of the product")
            ->notEmptyString("quantity", "Please provide quantity of the product ")
            ->add(
                "quantity",
                [
                    "numeric" => [
                        "rule" => "numeric",
                        'last' => true,
                        "message" => "Quantity must be numric",
                    ],
                ]
            );
        $validator
            ->requirePresence("product_photo", "create", "Please upload an Image for product ")
            ->notEmptyFile("product_photo", "Please upload an Image for product", "create")
            ->add(
                "product_photo",
                [
                    "extension" => [
                        'rule' => ['extension', ['png', 'jpeg', 'jpg', 'gif']],
                        'last' => true,
                        "message" => "File can be of type PNG,JPEG, JPG, GIF",
                    ],
                    "mimeType" => [
                        'rule' => [
                            'mimeType',
                            [
                                'image/png', 'image/jpeg', 'image/jpeg', 'image/jpeg', 'image/gif',
                            ],
                        ],
                        'last' => true,
                        'message' => "This mime type is not valid",
                    ],
                    "fileSize" => [
                        'rule' => ['fileSize', '<=', '3MB'],
                        'last' => true,
                        'message' => "File size must be lower then or equal to 1MB",
                    ],
                ]
            );
        $validator
            ->requirePresence("description", "Please provide description for product ")
            ->notEmptyString("description", "Please provide description for product ");

        return $validator;
    }
}
