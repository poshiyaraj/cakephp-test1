<?php
declare(strict_types=1);

namespace App\Policy;

use Authorization\IdentityInterface;

/**
 * Product policy
 */
class ProductPolicy
{
    /**
     * This method is used to check if user has aurhorization of create action
     *
     * @param \App\Policy\Authorization\IdentityInterface $currentUser instance of Authorization\IdentityInterface for current logged in user
     * @return bool
     */
    public function canCreate(IdentityInterface $currentUser)
    {
        return $this->isAdmin($currentUser);
    }

    /**
     * This method is used to check if user has aurhorization of update action
     *
     * @param \App\Policy\Authorization\IdentityInterface $currentUser instance of Authorization\IdentityInterface for current logged in user
     * @return bool
     */
    public function canUpdate(IdentityInterface $currentUser)
    {
        return $this->isAdmin($currentUser);
    }

    /**
     * This method is used to check if user has aurhorization of delete action
     *
     * @param \App\Policy\Authorization\IdentityInterface $currentUser instance of Authorization\IdentityInterface for current logged in user
     * @return bool
     */
    public function canDelete(IdentityInterface $currentUser)
    {
        return $this->isAdmin($currentUser);
    }

    /**
     * This method is used to check if user has aurhorization of edit action
     *
     * @param \App\Policy\Authorization\IdentityInterface $currentUser instance of Authorization\IdentityInterface for current logged in user
     * @return bool
     */
    public function canEdit(IdentityInterface $currentUser)
    {
        return $this->isAdmin($currentUser);
    }

    /**
     * This method is used to check if has admin role or not
     *
     * @param \App\Policy\Authorization\IdentityInterface $currentUser instance of Authorization\IdentityInterface for current logged in user
     * @return bool
     */
    protected function isAdmin(IdentityInterface $currentUser)
    {
        return $currentUser->role_id === 1;
    }
}
